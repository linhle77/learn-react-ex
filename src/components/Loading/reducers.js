import { START_LOADING, DONE_LOADING } from './constants';
const initialState = {
    isLoading: 0
}

function Loading(state = initialState, action) {
    switch (action.type) {
        case START_LOADING:
            return {
                ...state,
                isLoading: state.isLoading + 1
            }
        case DONE_LOADING:
            return {
                ...state,
                isLoading: state.isLoading - 1
            }
        default:
            return state
    }
}

export default Loading;