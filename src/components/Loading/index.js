import React from 'react';
import './style.scss';

const Loading = () => {
  return (
    <div className="spinner-wrapper">
      <span className="spinner"/>
    </div>
  );
};

export default Loading;
