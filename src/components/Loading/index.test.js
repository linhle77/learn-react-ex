import React from 'react';
import { shallow } from 'enzyme';

import Loading from './index';

describe('<Loading />', () => {
  it('should render a div', () => {
    const renderedComponent = shallow(<Loading />);
    expect(renderedComponent.find('div')).toHaveLength(1);
  });
});