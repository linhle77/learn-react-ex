import { START_LOADING, DONE_LOADING } from './constants';

export const startLoading = () => dispatch => dispatch({
  type: START_LOADING
});

export const doneLoading = () => dispatch => dispatch({
  type: DONE_LOADING
});

