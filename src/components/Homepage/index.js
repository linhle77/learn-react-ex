import React from 'react';

const HomePage = () => {
  return (
    <div className="container">
      <h1>
        Learn React From beginner to advanced
      </h1>
    </div>
  );
};

export default HomePage;
