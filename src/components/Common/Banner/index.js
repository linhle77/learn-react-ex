import React from 'react';
import BannerImg from './banner_detail.jpg';
import './style.scss';

const Banner = () => {
    return (
        <div className='banner' style={{backgroundImage: `url(${BannerImg})`}}>
        </div>
    );
};

export default Banner;