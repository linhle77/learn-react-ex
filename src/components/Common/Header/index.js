import React, {Fragment} from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import Loading from '../../Loading';
import './style.scss';

class Header extends React.Component {
    render() {
        const activeStyle = { color: '#1ab8d3' };
        const { isLoading } = this.props;
        return (
            <Fragment>
                <header>
                    <div className='menu'>
                        <div className='container'>
                            <NavLink exact to='/lesson1' activeStyle={activeStyle}>Lesson 1</NavLink>
                            <NavLink exact to='/lesson2' activeStyle={activeStyle}>Lesson 2</NavLink>
                            <NavLink exact to='/news' activeStyle={activeStyle}>News</NavLink>
                            <NavLink exact to='/filter' activeStyle={activeStyle}>Filter</NavLink>
                        </div>
                    </div>
                </header>
                {isLoading > 0 && <Loading />}
            </Fragment>
        )
    }
}

function mapStatetoProps(state) {
    const { Loading: { isLoading } } = state;
    return {
        isLoading
    }
}

Header.propTypes = {
    isLoading: PropTypes.number
}

export default connect(
    mapStatetoProps
)(Header);
