import React from 'react';
import ListUsers from './ListUsers';

const Lesson2 = () => {
  return (
    <div className="container">
      <h1 className="alt-header">Lesson 2 - List Users</h1>
      <ListUsers />
    </div>
  );
};

export default Lesson2;
