import { START_LOADING, DONE_LOADING, LOAD_USERS_SUCCESS, itemsPerPage} from './constants';

const startLoading = () => ({
  type: START_LOADING
});

const doneLoading = () => ({
  type: DONE_LOADING
});

const loadUsersSuccess = (users, total) => ({
  type: LOAD_USERS_SUCCESS,
  users,
  total
});


export const loadUsers = (page) => (dispatch) => {
  dispatch(startLoading());
  fetch(`https://reqres.in/api/users?page=${page}&per_page=${itemsPerPage}`)
    .then((res) => {
      return res.json();
    })
    .then((json) => {
      dispatch(loadUsersSuccess(json.data, json.total));
      dispatch(doneLoading());
    })
    .catch((error) => {
      throw error;
    });
}