import React from 'react';
import { Table as BsTable } from 'react-bootstrap';
import Pagination from "react-js-pagination";
import Loading from '../../Loading';
import Row from './Row';
import { itemsPerPage } from './constants';
import './style.scss';
import { UsersTableProps } from './types';

export default class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
    }
  }

  componentDidMount() {
     this.props.actions.loadUsers(1);
  }

  handlePageChange = (page) => {
    this.setState({
      activePage: page
    })
    this.props.actions.loadUsers(page);
  }

  render() {
    const { users, isLoading, total } = this.props;
    const { activePage } = this.state;
    const loading = (<Loading/>);
    const emptyData = (<tr><td colSpan="4" className="text-center">No User Found</td></tr>);

    return (
      <div>
        {isLoading && loading}
        <BsTable>
          <thead>
            <tr>
              <th>ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          {
            users.length ?
            users.map((user, index) => (
              <Row key={index} user={user}/>
            )) : emptyData
          }
          </tbody>
        </BsTable>
        <Pagination
          itemClass="pageNumber"
          hideFirstLastPages
          itemClassPrev="pagePrev"
          itemClassNext="pageNext"
          activePage={activePage}
          itemsCountPerPage={itemsPerPage}
          totalItemsCount={total}
          onChange={this.handlePageChange}
        />
      </div>
    );
  }
}

Table.propTypes = UsersTableProps;

Table.defaultProps = {
  data: []
}
