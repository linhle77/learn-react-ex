import { LOAD_NEWS_DETAIL_SUCCESS } from './constants';
const initialState = {
    news: null
}

function News (state = initialState, action) {
    switch (action.type) {
        case LOAD_NEWS_DETAIL_SUCCESS:
            return {
                ...state,
                news: action.news
            }
        default:
            return state
    }
}

export default News;
