import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "./actions";

import NewsDetail from "./NewsDetail";

function mapStatetoProps(state) {
    const { NewsDetail: {news}} = state;
    return {
        news
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(
    mapStatetoProps,
    mapDispatchToProps
)(NewsDetail);
