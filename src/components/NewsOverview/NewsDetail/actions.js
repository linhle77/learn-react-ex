import { LOAD_NEWS_DETAIL_SUCCESS} from './constants';
import { startLoading, doneLoading } from '../../Loading/actions'

const loadNewsDetailSuccess = (news) => ({
  type: LOAD_NEWS_DETAIL_SUCCESS,
  news
});


export const loadNewsDetail = (id) => (dispatch) => {
  startLoading(dispatch);
  fetch(`http://sb.mijnstaging.nl/wp-json/wp/v2/posts/${id}`)
    .then((res) => {
      return res.json();
    })
    .then((json) => {
      dispatch(loadNewsDetailSuccess(json));
      doneLoading(dispatch);
    })
    .catch((error) => {
      throw error;
    });
}
