import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import Loading from '../../Loading';
import Banner from '../../Common/Banner';
import './style.scss';

export default class News extends React.Component {
    componentDidMount() {
        const { news, match } = this.props;
        if (match) {
            const id = match.params.id;
            if (!news) {
                this.props.actions.loadNewsDetail(id);
            }
        }
    }

    decodeHtml(html) {
        var txt = document.createElement('textarea');
        txt.innerHTML = html;
        return txt.value;
    }

    render() {
        const { news } = this.props;
        return (
            <Fragment>
            { !news ?
                <Loading />
                :
                <div className='contentPage NewsDetail'>
                    <Banner />
                    <div className='outContainer'>
                        <div className='container'>
                            <div className='row'>
                                <div className='col-sm-3 RelatedArticles hidden-sm hidden-xs'>
                                    <ul>
                                        <li className='current-menu-active'>
                                            <a href='http://forten.stagingsite.nl/gezocht-stagiaire-en-vrijwilligers-forten-floral-art/'>Gezocht: stagiaire en vrijwilligers voor Forten &amp; Floral Art</a>
                                        </li>
                                        <li>
                                            <a href='http://forten.stagingsite.nl/mooi-linieland/'>Mooi Linieland! Toekomstig UNESCO-landschap tussen de forten</a>
                                        </li>
                                        <li>
                                            <a href='http://forten.stagingsite.nl/escaperoom/'>Extra spannend: escape room op een fort!</a>
                                        </li>
                                        <li>
                                            <a href='http://forten.stagingsite.nl/duurzame-menukaart-forten-kastelen-en-vestingsteden/'>Duurzame menukaart voor forten, kastelen en vestingsteden</a>
                                        </li>
                                        <li>
                                            <a href='http://forten.stagingsite.nl/vier-lente-op-eiland-schalkwijk/'>Vier de lente op het Eiland van Schalkwijk</a>
                                        </li>
                                        <li><a href='http://forten.stagingsite.nl/category/nieuws/'>Meer Nieuws</a></li>
                                    </ul>
                                </div>
                                <div className='col-sm-9 hidden-sm hidden-xs'>
                                    <div className='Title'>{this.decodeHtml(news.title.rendered)}</div>
                                    <p className='Date'>14-06-2017</p>
                                    <div className='Text wp-editor'>
                                        <img width='370' height='247' src={news.imageURL} className='alignright' alt='' />
                                        <p>{this.decodeHtml(news.content.rendered.replace(/(<([^>]+)>)/ig, ''))}</p>
                                        <p>{this.decodeHtml(news.excerpt.rendered.replace(/(<([^>]+)>)/ig, ''))}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
            </Fragment>
        );
    }
}

News.propTypes = {
    news: PropTypes.object,
    actions: PropTypes.object
}
