import React from 'react';
import {Link} from "react-router-dom";
import { NewsProps } from './types';
import TextTruncate from 'react-text-truncate';

const DEFAULT_IMAGE = 'https://forten.nl/wp-content/uploads/2019/02/Fort-Abcoude-rondleiding-1000-pix-270x197.jpg';

export default class NewsItem extends React.Component {
  constructor(props) {
    super(props);
  }

  decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
  }

  render() {
    const { news } = this.props;
    const itemDate = new Date(news.date);
    let month = itemDate.getMonth() + 1;
    month = month < 10 ? "0"+month : month;
    let date = itemDate.getDate();
    date = date < 10 ? "0" + date : date;

    const imageURL = news.imageURL || DEFAULT_IMAGE;
    return (
      <Link className="item col-sm-4  col-md-3" to={`/posts/${news.id}`}>
        <img width="270" height="197" src={imageURL} className="itemImage" alt={this.decodeHtml(news.title.rendered)} />
        <h2>{this.decodeHtml(news.title.rendered)}</h2>
        <div className="date" > {`${date}-${month}-${itemDate.getFullYear()}`}</div>
        <div className="ingress">
          <TextTruncate
            line={3}
            truncateText="[…]"
            text={this.decodeHtml(news.excerpt.rendered.replace(/(<([^>]+)>)/ig, ""))}
          />
        </div>
      </Link>
    );
  }
}

NewsItem.propTypes = NewsProps;

NewsItem.defaultProps = {
  news: {}
};