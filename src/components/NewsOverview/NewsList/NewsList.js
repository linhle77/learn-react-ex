import React from 'react';
import Pagination from "react-js-pagination";
import NewsItem from './NewsItem';
import Banner from "../../Common/Banner"
import './style.scss';
import { NewsListProps } from './types';
import  Table from '../../Lesson2/ListUsers/Table';

const itemsPerPage = 4;

export default class NewsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      start: 0,
      end: itemsPerPage
    };

  }

  componentDidMount() {
    const { newsItems } = this.props;
    if (newsItems.length <= 0) {
      this.props.actions.loadNewsList();
    }
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber, start: (itemsPerPage * (pageNumber - 1)), end: (itemsPerPage * pageNumber) });
  }

  render() {
    const { newsItems } = this.props;
    const { start, end } = this.state;
    const emptyData = (<div className="text-center">No News Found</div>);
    const itemsAfterPaging = newsItems.slice(start, end);
    return (
      <div className="contentPage">
        <Banner/>
        <div className="outContainer">
          <aside id="NewsOverview" className="paddingBottom50">
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <div className="title"><span className="icon"></span>Nieuws</div>
                </div>
              </div>
              <div className="row NewsList">
                {
                  itemsAfterPaging.length ?
                  itemsAfterPaging.map((news, index) => (
                    <NewsItem key={index} news={news}/>
                  )) : emptyData
                }
              </div>
              <div className="row">
                <div className="col">
                  <Pagination
                    itemClass="pageNumber"
                    prevPageText="< Vorige"
                    nextPageText="Volgende >"
                    hideFirstLastPages
                    itemClassPrev="pagePrev"
                    itemClassNext="pageNext"
                    activePage={this.state.activePage}
                    itemsCountPerPage={itemsPerPage}
                    totalItemsCount={newsItems.length}
                    onChange={this.handlePageChange}
                  />
                </div>
              </div>
            </div>
          </aside>
          <a onClick={this.loadNewsList1} href="javascript:;">Show List Users</a>
        </div>
      </div>
    );
  }
}

NewsList.propTypes = NewsListProps;

NewsList.defaultProps = {
  newsList: []
}
