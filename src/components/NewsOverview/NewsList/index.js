import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from './actions';
import newsList from './NewsList';

function mapStateToProps(state) {
  const { newsList: { newsItems }} = state;
  // const { news: { data, isLoading }, Users: { data, isLoading}} = state;
  return {
    newsItems,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(newsList);
