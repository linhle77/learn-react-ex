import {
  arrayOf,
  shape,
  number,
  string,
  func,
  bool,
} from 'prop-types';

const newsShape = shape({
  id: number,
  first_name: string,
  last_name: string,
  avatar: string
})

export const NewsProps = {
  news: newsShape
}

export const NewsListProps = {
  news: arrayOf(newsShape)
}