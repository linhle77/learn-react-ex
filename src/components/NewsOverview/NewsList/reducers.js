import { LOAD_NEWS_LIST_SUCCESS, LOAD_NEWS_LIST_SUCCESS1 } from './constants';
const initialState= {
    newsItems: [],
    users: []
}

function NewsOverview (state = initialState, action) {
  switch (action.type) {
    case LOAD_NEWS_LIST_SUCCESS:
      return {
        ...state,
        newsItems: action.newsItems
      }
    case LOAD_NEWS_LIST_SUCCESS1:
      return {
        ...state,
        users: action.users
      }
    default:
      return state
  }
}

export default NewsOverview;
