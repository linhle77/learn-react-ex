import { LOAD_NEWS_LIST_SUCCESS, LOAD_NEWS_LIST_SUCCESS1} from './constants';
import { startLoading, doneLoading } from '../../Loading/actions';

const loadNewsListSuccess = (newsItems) => ({
  type: LOAD_NEWS_LIST_SUCCESS,
  newsItems
});

export const loadNewsList = () => (dispatch) => {
  dispatch(startLoading());
  fetch('http://sb.mijnstaging.nl/wp-json/wp/v2/posts?per_page=100')
    .then((res) => {
      return res.json();
    })
    .then((json) => {
      dispatch(loadNewsListSuccess(json));
      dispatch(doneLoading());
    })
    .catch((error) => {
      throw error;
    });
}


