import React, {Component} from 'react';

export default class SideBar extends Component {
    render() {
        return (
            <div className="sidebar-filter">
				<div className="inner">
                    <h2 className="main-title">Filters</h2>
                    <div className="content-filter">
                        <form id="event-filter-form" action="" method="post" autoComplete="on">
                            <div className="option-filter">
                                <p className="title">Routes</p>
                                <div className="option-content">
                                    <div className="filter-wrapper" filter-query-type="tax" data-filter-origin="event" data-filter-type="checkbox">
                                        <ul className="checkbox-group filter-group" data-filter-slug="cat_event_routes" data-filter-name="routes">
                                            <li>
                                                <input type="checkbox" id="checkbox0" className="checkbox" data-term-id="100" name="all-routes"/>
                                                <label htmlFor="checkbox0">Alle routes</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="checkbox1" className="checkbox" data-term-id="78" name="fietsroute"/>
                                                <label htmlFor="checkbox1">Fietsroute</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="checkbox2" className="checkbox" data-term-id="79" name="wandelroute"/>
                                                <label htmlFor="checkbox2">Wandelroute</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="checkbox3" className="checkbox" data-term-id="80" name="vaarroute"/>
                                                <label htmlFor="checkbox3">Vaarroute</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="checkbox4" className="checkbox" data-term-id="88" name="paardrijroute"/>
                                                <label htmlFor="checkbox4">Paardrijroute</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="checkbox5" className="checkbox" data-term-id="89" name="autoroute"/>
                                                <label htmlFor="checkbox5">Autoroute</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                                <div className="option-filter">
                                    <p className="title">Activiteiten</p>
                                    <div className="option-content">
                                        <div className="filter-wrapper" filter-query-type="tax" data-filter-origin="event" data-filter-type="checkbox">
                                            <ul className="checkbox-group filter-group" data-filter-slug="cat_event" data-filter-name="activiteiten">
                                                                                <li>
                                                    <input type="checkbox" id="checkbox6" className="checkbox" data-term-id="95" name="all-activiteiten"/>
                                                    <label htmlFor="checkbox6">Alle activiteiten</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox7" className="checkbox" data-term-id="43" name="eten-en-drinken"/>
                                                    <label htmlFor="checkbox7">Eten en drinken</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox8" className="checkbox" data-term-id="46" name="kunst-en-cultuur"/>
                                                    <label htmlFor="checkbox8">Kunst en Cultuur</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox9" className="checkbox" data-term-id="42" name="kids"/>
                                                    <label htmlFor="checkbox9">Kids</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox10" className="checkbox" data-term-id="44" name="naar-buiten"/>
                                                    <label htmlFor="checkbox10">Naar Buiten</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox11" className="checkbox" data-term-id="61" name="overnachten"/>
                                                    <label htmlFor="checkbox11">Overnachten</label>
                                                </li>
                                                                            </ul>
                                        </div>
                                                            </div>
                                </div>

                                <div className="option-filter">
                                    <p className="title">Verhuur</p>
                                    <div className="option-content">
                                        <div className="filter-wrapper" filter-query-type="tax" data-filter-origin="event" data-filter-type="checkbox">
                                            <ul className="checkbox-group filter-group" data-filter-slug="cat_event" data-filter-name="verhuur">
                                                <li>
                                                    <input type="checkbox" id="checkbox12" className="checkbox" data-term-id="98" name="all-verhuur"/>
                                                    <label htmlFor="checkbox12">Alle verhuur</label>
                                                </li>
                                                <li>
                                                    <input type="checkbox" id="checkbox13" className="checkbox" data-term-id="83" name="groeps-en-bedrijfsuitjes"/>
                                                    <label htmlFor="checkbox13">Groeps- en bedrijfsuitjes</label>
                                                </li>
                                                <li>
                                                    <input type="checkbox" id="checkbox14" className="checkbox" data-term-id="60" name="vergaderen-en-congres"/>
                                                    <label htmlFor="checkbox14">Vergaderen en Congres</label>
                                                </li>
                                                <li>
                                                    <input type="checkbox" id="checkbox15" className="checkbox" data-term-id="62" name="trouwen-en-feesten"/>
                                                    <label htmlFor="checkbox15">Trouwen en feesten</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div className="option-filter">
                                    <p className="title">Evenementen</p>
                                    <div className="option-content">
                                        <div className="filter-wrapper" filter-query-type="tax" data-filter-origin="event" data-filter-type="checkbox">
                                            <ul className="checkbox-group filter-group" data-filter-slug="cat_event" data-filter-name="evenementen">
                                                                                <li>
                                                    <input type="checkbox" id="checkbox16" className="checkbox" data-term-id="96" name="all-evenementen"/>
                                                    <label htmlFor="checkbox16">Alle evenementen</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox17" className="checkbox" data-term-id="45" name="start-fortenseizoen"/>
                                                    <label htmlFor="checkbox17">Start Fortenseizoen</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox18" className="checkbox" data-term-id="81" name="mooi-linieland"/>
                                                    <label htmlFor="checkbox18">Mooi Linieland</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox19" className="checkbox" data-term-id="64" name="fortenfestival"/>
                                                    <label htmlFor="checkbox19">Fortenfestival</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox20" className="checkbox" data-term-id="75" name="nachtvandenacht"/>
                                                    <label htmlFor="checkbox20">Nacht van de Nacht</label>
                                                </li>
                                                                            </ul>
                                        </div>
                                                            </div>
                                </div>

                                <div className="option-filter">
                                    <p className="title">Waterlinie</p>
                                    <div className="option-content">
                                        <div className="filter-wrapper" filter-query-type="tax" data-filter-origin="event" data-filter-type="checkbox">
                                            <ul className="checkbox-group filter-group" data-filter-slug="cat_fort" data-filter-name="waterlinie">
                                                                                <li>
                                                    <input type="checkbox" id="checkbox21" className="checkbox" data-term-id="47" name="nieuwe-hollandse-waterlinie"/>
                                                    <label htmlFor="checkbox21">Nieuwe Hollandse Waterlinie</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox22" className="checkbox" data-term-id="48" name="oude-hollandse-waterlinie"/>
                                                    <label htmlFor="checkbox22">Oude Hollandse Waterlinie</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox23" className="checkbox" data-term-id="49" name="stelling-van-amsterdam"/>
                                                    <label htmlFor="checkbox23">Stelling van Amsterdam</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox24" className="checkbox" data-term-id="50" name="zuiderwaterlinie"/>
                                                    <label htmlFor="checkbox24">Zuiderwaterlinie</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox25" className="checkbox" data-term-id="51" name="grebbelinie"/>
                                                    <label htmlFor="checkbox25">Grebbelinie</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox26" className="checkbox" data-term-id="52" name="stelling-van-den-helder"/>
                                                    <label htmlFor="checkbox26">Stelling van Den Helder</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox27" className="checkbox" data-term-id="53" name="friese-waterlinie"/>
                                                    <label htmlFor="checkbox27">Friese Waterlinie</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox28" className="checkbox" data-term-id="66" name="staats-spaanse-linie"/>
                                                    <label htmlFor="checkbox28">Staats-Spaanse Linie</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox29" className="checkbox" data-term-id="67" name="atlanktikwall"/>
                                                    <label htmlFor="checkbox29">Atlanktikwall</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox30" className="checkbox" data-term-id="74" name="werken-aan-de-westerschelde"/>
                                                    <label htmlFor="checkbox30">Werken aan de Westerschelde</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox31" className="checkbox" data-term-id="82" name="ijssellinie"/>
                                                    <label htmlFor="checkbox31">IJssellinie</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox32" className="checkbox" data-term-id="87" name="divers"/>
                                                    <label htmlFor="checkbox32">Divers</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox33" className="checkbox" data-term-id="122" name="stelling-van-utrecht"/>
                                                    <label htmlFor="checkbox33">Stelling van Utrecht</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox34" className="checkbox" data-term-id="123" name="stelling-van-de-monden-van-de-maas-en-het-haringvliet"/>
                                                    <label htmlFor="checkbox34">Stelling van de Monden van de Maas en het Haringvliet</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox35" className="checkbox" data-term-id="124" name="stelling-van-het-hollandsch-diep-en-het-volkerak"/>
                                                    <label htmlFor="checkbox35">Stelling van het Hollandsch Diep en het Volkerak</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox36" className="checkbox" data-term-id="125" name="vestingwerken-van-maastricht"/>
                                                    <label htmlFor="checkbox36">Vestingwerken van Maastricht</label>
                                                </li>
                                                                            </ul>
                                        </div>
                                                            </div>
                                </div>

                                <div className="option-filter">
                                    <p className="title">Prijs</p>
                                    <div className="option-content">
                                        <div className="filter-wrapper" filter-query-type="tax" data-filter-origin="event" data-filter-type="checkbox">
                                            <ul className="checkbox-group filter-group" data-filter-slug="cat_event_price" data-filter-name="prijs">
                                                                                <li>
                                                    <input type="checkbox" id="checkbox37" className="checkbox" data-term-id="101" name="gratis"/>
                                                    <label htmlFor="checkbox37">Gratis</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox38" className="checkbox" data-term-id="102" name="betaald"/>
                                                    <label htmlFor="checkbox38">Betaald</label>
                                                </li>
                                                                                <li>
                                                    <input type="checkbox" id="checkbox39" className="checkbox" data-term-id="118" name="nvt"/>
                                                    <label htmlFor="checkbox39">Nvt</label>
                                                </li>
                                                                            </ul>
                                        </div>
                                                            </div>
                                </div>
                                    </form>
                    </div>
                </div>
                            </div>
        )
    }
}