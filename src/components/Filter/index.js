import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from './actions';
import Filter from './Filter';

function mapStateToProps(state) {
    return state
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Filter);
