import React, { Component } from 'react';
import SideBar from './Sidebar';
import './style.scss';

export default class Filter extends Component {
    state = {
        data: '',
    }
    componentDidMount() {
        const details = {
            action: 'sf_ajax_load_posts',
            dataType: 'json',
            page_id: 269,
            offset: 0,
            next_page: 1,
            geo_radius: 5,
        };
        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch('http://forten.stagingsite.nl/wp-admin/admin-ajax.php', {
            method: 'POST',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
            body: formBody
        })
        .then((res) => {
            return res.json();
        })
        .then((json) => {
            this.setState({data: json.data})
        })
    }

    render() {
        const { data } = this.state;
        return(
            <div className='container'>
                <div className="row">
                    <div className='col-xs-12 col-sm-5 col-lg-3'>
                        <SideBar/>
                    </div>
                    <div className='col-xs-12 col-sm-7 col-lg-9 right-side'>
                        <div id='activities-result'>
                            {data && <div className='row' dangerouslySetInnerHTML={{ __html: data }}></div>}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
