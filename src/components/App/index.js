/* eslint-disable import/no-named-as-default */
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import Header from '../Common/Header';
import Lesson1 from '../Lesson1';
import Lesson2 from '../Lesson2';
import UserDetail from '../Lesson2/UserDetail';
import NotFoundPage from '../NotFoundPage';
import Homepage from '../Homepage';
import NewsOverview from '../NewsOverview';
import NewsDetail from '../NewsOverview/NewsDetail';
import Filter from '../Filter';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.scss';

class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path='/' component={Homepage} />
          <Route exact path='/lesson1' component={Lesson1} />
          <Route exact path='/lesson2' component={Lesson2} />
          <Route exact path='/users/:id' component={UserDetail} />
          <Route exact path='/posts/:id' component={NewsDetail} />
          <Route exact path='/news' component={NewsOverview} />
          <Route exact path='/filter' component={Filter} />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
    );
  }
}

export default hot(module)(App);
