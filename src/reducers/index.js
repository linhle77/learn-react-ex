import { combineReducers } from 'redux';
import Lesson1 from '../components/Lesson1/reducers';
import ListUsers from '../components/Lesson2/ListUsers/reducers';
import UserDetail from '../components/Lesson2/UserDetail/reducers';
import newsList from '../components/NewsOverview/NewsList/reducers';
import NewsDetail from '../components/NewsOverview/NewsDetail/reducers';
import Loading from '../components/Loading/reducers';

const rootReducer = combineReducers({
  Lesson1,
  ListUsers,
  UserDetail,
  newsList,
  NewsDetail,
  Loading
});

export default rootReducer;
